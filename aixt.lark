// This file is part of the Aixt project, https://gitlab.com/fermarsan/aixt-project.git
//
// The MIT License (MIT)
// 
// Copyright (c) 2023 Fernando Martínez Santa
//
// Based on the V grammar taken from these sources (ordered by importance):
//     Abstract Syntax Tree V's documentation:  https://modules.vlang.io/v.ast.html
//     V grammar definition in Antlr-v4:        https://github.com/antlr/grammars-v4/blob/master/v/V.g4
//     V grammar definition in lark:            https://github.com/Itay2805/Vork/blob/master/v.lark


!source_file: top_decl_list? stmt_list?


!top_decl_list: top_decl (";"? top_decl)*

!top_decl:  C_PREPROC
        |   import_stmt
        |   fn_decl
        |   const_decl

!import_stmt:   "import" IDENT ("{" IDENT ("," IDENT)* "}")? 

!fn_decl: ATTRIB? "fn" IDENT "(" param_list? ")" fn_return? block

!fn_return: TYPE_NAME

!const_decl:    "const" "(" const_item const_item* ")"
            |   "const" const_item

!const_item:    IDENT "=" expr


!stmt_list:     (stmt (";"? stmt)*) ";"?

!stmt:  expr                                                //->  expr_stmt
    |   block
    |   assign_stmt
    |   "break"
    |   "return" expr                                       ->  return_stmt                                       
    |   "for" block                                         ->  for_bare_stmt
    |   "for" expr block                                    ->  for_cond_stmt
    |   "for" assign_stmt ";" expr ";" assign_stmt block    ->  for_c_stmt
    |   "for" IDENT "in" range_expr block                   ->  for_in_stmt


!assign_stmt:   expr_list ASSIGN_OP expr_list       -> simple_assign_stmt
            |   expr_list ":=" expr_list            -> decl_assign_stmt   
            |   expr_list ":=" "[" expr_list "]"    -> array_init       
            |   expr INC_DEC_OP                     -> inc_dec_stmt

!block: "{" stmt_list "}"

!param_list:    param ( "," param )*

!param:         expr TYPE_NAME 
    
!expr_list:     expr ( "," expr )*

!expr:  UNARY_OP expr           //->  unary_expr
    |   expr BINARY_OP expr     //->  binary_expr
    |   "(" expr ")"            //->  par_expr
    |   expr "[" literal "]"    ->  index_expr 
    |   expr "(" expr_list ")"  ->  call_expr 
    |   TYPE_NAME "(" expr ")"  ->  cast_expr  
    |   IDENT "." IDENT         //->  qualified_ident
    |   range_expr
    |   if_expr
    |   IDENT
    |   literal   

!range_expr:    expr ".." expr

!if_expr:   "if" expr block ("else" (block | if_expr))?


!literal:   INTEGER_LITERAL     ->  integer_literal 
        |   FLOAT_LITERAL       ->  float_literal
        |   BOOL_LITERAL        ->  bool_literal
        |   CHAR_LITERAL        ->  char_literal 
        |   STRING_LITERAL      ->  string_literal    

ATTRIB: "[" IDENT "]"

TYPE_NAME.1:    "rune" | "bool" | "string" | NUMERIC_TYPE | "mutex"

NUMERIC_TYPE.1: "u8" | "u16" | "u32" | "u64" | "usize" //| "uint"
            |   "i8"  | "i16" | "i32" | "i64" | "isize" | "int"
            |   "f64" | "f32"

ASSIGN_OP:      ("+" | "-" | "|" | "^" | "*" | "/" | "%" | "<<" | ">>" | "&" )? "="
BINARY_OP:      "||" | "&&" | REL_OP | ADD_OP | MUL_OP
REL_OP:         "==" | "!=" | "<" | "<=" | ">" | ">="
ADD_OP.1:       "+" | "-" | "|" | "^"
MUL_OP:         "*" | "/" | "%" | "<<" | ">>" | "&" //| "&^"
UNARY_OP:       "+" | "-" | "!" | "^" | "*" | "&" //| "<-"
INC_DEC_OP.2:   "++" | "--"

FLOAT_LITERAL:      INT EXP | DECIMAL EXP?
INTEGER_LITERAL:    INT | HEX | OCTAL | BIN
DECIMAL:            INT "." INT? | "." INT

BOOL_LITERAL.1:   "true" | "false"
STRING_LITERAL: /\'.*\'/
CHAR_LITERAL:   /`.`/
EXP.1:          /[eE][\+-]?[0-9][0-9_]*/            // all numeric literals with "_"
INT:            /[0-9][0-9_]*/                        
HEX.1:          /0x[0-9A-Fa-f][0-9A-Fa-f_]*/
OCTAL.1:        /0o[0-7][0-7_]*/        
BIN.1:          /0b[01][01_]*/
C_PREPROC.1:    /#.+\n/                


%import common.CNAME    ->  IDENT
%import common (CPP_COMMENT, C_COMMENT, WS_INLINE, NEWLINE)

%ignore WS_INLINE 
%ignore CPP_COMMENT
%ignore C_COMMENT
%ignore NEWLINE

// type expr = 
//    AnonFn
// 	| ArrayDecompose
// 	                        | array_init
// 	| AsCast
// 	| Assoc
// 	| At_expr
// 	                        | BOOL_LITERAL
// 	| CTempVar
// 	                        | call_expr
// 	                        | Cast_expr
// 	| Chan_init
// 	                        | CHAR_LITERAL
// 	                        | Comment (CPP_COMMENT, C_COMMENT)
// 	| ComptimeCall
// 	| ComptimeSelector
// 	| Comptime_type
// 	| Concat_expr
// 	| Dump_expr
// 	| Empty_expr
// 	| EnumVal
// 	                        | FLOAT_LITERAL
// 	| Go_expr
// 	                        | IDENT      
// 	                        | if_expr
// 	| IfGuard_expr
// 	                        | index_expr
// 	| Infix_expr
// 	                        | INTEGER_LITERAL
// 	| IsRef_type
// 	| Likely
// 	| Lock_expr
// 	| Map_init
// 	| Match_expr_expr
// 	| None
// 	| OffsetOf
// 	| Or_expr
// 	                        | par_expr
// 	| Postfix_expr
// 	| Prefix_expr
// 	                        | range_expr
// 	| Select_expr
// 	| Selector_expr
// 	| SizeOf
// 	| Sql_expr
// 	| STRING_LITERALInter_literaleral
// 	                        | STRING_LITERAL
// 	| Struct_init
// 	| _typeNode
// 	| _typeOf
// 	| Unsafe_expr

// type _stmt = Asm_stmt
// 	| Assert_stmt
// 	                        | assign_stmt
// 	                        | block
// 	| Branch_stmt
// 	| ComptimeFor
// 	                        | const_decl
// 	| Defer_stmt
// 	| Empty_stmt
// 	| Enum_decl
// 	                        | expr_stmt
// 	                        | fn_decl
// 	                        | for_c_stmt
// 	                        | for_in_stmt
// 	                        | for_stmt (for_bare_stmt, for_cond_stmt)
// 	| Global_decl
// 	| GotoLabel
// 	| Goto_stmt
// 	| Hash_stmt
// 	                        | import_stmt
// 	| Interface_decl
// 	| Module
// 	| NodeError
// 	                        | return_stmt
// 	| Sql_stmt
// 	| Struct_decl
// 	| _type_decl