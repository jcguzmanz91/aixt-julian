a := 10
b := 23

for a > 0 {
	b += a
	a -= 1
}
