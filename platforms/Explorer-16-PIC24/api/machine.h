// This file is part of the Aixt project, https://gitlab.com/fermarsan/aixt-project.git
//
// The MIT License (MIT)
// 
// Copyright (c) 2022 Fernando Martínez Santa
 
#ifndef _MACHINE_H_
#define _MACHINE_H_

#include "./machine/pin.h"
#include "./machine/uart.h"

#endif  //_MACHINE_H_ 